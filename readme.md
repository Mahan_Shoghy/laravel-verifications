### Installation

```shell
composer require mahan-shoghy/laravel-verifications
```

```shell
php artisan vendor:publish --provider="MahanShoghy\LaravelVerification\VerificationServiceProvider" --tag="config"
```

We use <a href="https://github.com/tzsk/sms">Laravel SMS Gateway</a> for SMS handling, so you can configure your sms via this package
