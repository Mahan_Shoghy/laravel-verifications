<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use MahanShoghy\LaravelVerification\Enums\VerificationTypeEnum;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create(config('verification.table'), function (Blueprint $table) {
            $table->id();
            $table->enum('type', VerificationTypeEnum::values());
            $table->string('value');
            $table->string('token');
            $table->timestamp('created_at');
            $table->timestamp('expired_at');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists(config('verification.table'));
    }
};
