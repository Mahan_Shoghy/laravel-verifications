<?php

namespace MahanShoghy\LaravelVerification\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Carbon;
use MahanShoghy\LaravelVerification\Enums\VerificationTypeEnum;
use MahanShoghy\LaravelVerification\Types\TypeInterface;

class Verification extends Model
{
    public $timestamps = false;

    protected $fillable = ['token', 'value', 'type'];

    protected $casts = [
        'type' => VerificationTypeEnum::class,
        'token' => 'encrypted',
        'created_at' => 'date',
        'expired_at' => 'date'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->table = config('verification.table') ?: parent::getTable();
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
            $model->expired_at = now()->addSeconds(config('verification.expiration_seconds'));
        });
    }

    /**
     * Generate token with specific length
     *
     * @param null|int $length
     */
    public static function generateToken(int $length = null): \Generator
    {
        $length ??= config('verification.token_length');

        for($i = 0; $i < $length; $i++) {
            yield mt_rand(0, 9);
        }
    }

    /**
     * Retrieve active token
     *
     * @param string $value
     * @param VerificationTypeEnum $type
     * @param null|int $length
     * @return Verification
     */
    public static function getToken(string $value, VerificationTypeEnum $type, int $length = null): Verification
    {
        $token = self::validToken($value, $type);

        if (!$token) {
            $token = self::generateToken($length);
            $token = iterator_to_array($token);
            $token = implode('', $token);

            /**
             * @var Verification $token
             */
            $token = self::query()->create([
                'type'  => $type,
                'value' => $value,
                'token' => $token,
            ]);

            $token->fresh = true;
        }


        return $token;
    }

    /**
     * Retrieve valid token if exists
     *
     * @param string $value
     * @param VerificationTypeEnum $type
     * @return null|Verification
     */
    public static function validToken(string $value, VerificationTypeEnum $type): ?Verification
    {
        /**
         * @var Verification $token
         */
        $token = self::query()
            ->where('type', $type)
            ->where('value', $value)
            ->where('expired_at', '>', now())
            ->latest('created_at')
            ->first();

        return $token;
    }

    /**
     * Check is to many attempts in specific datetime
     *
     * @param string $value
     * @param VerificationTypeEnum $type
     * @param Carbon|null $start_date default: start of day
     * @param Carbon|null $end_date default: end of day
     * @param int|null $max_attempt
     * @return bool if more than
     */
    public static function isTooManyAttempt(string $value, VerificationTypeEnum $type, Carbon $start_date = null, Carbon $end_date = null, int $max_attempt = null): bool
    {
        $start_date ??= now()->startOfDay();
        $end_date ??= now()->endOfDay();
        $max_attempt ??= config('verification.max_attempt');

        $attempt = self::query()
            ->where('type', $type)
            ->where('value', $value)
            ->whereBetween('created_at', [$start_date, $end_date])
            ->count();

        if ($attempt < $max_attempt) {
            return false;
        }

        return true;
    }

    /**
     * Send valid token to value for specific type
     *
     * @param string $value
     * @param VerificationTypeEnum $type
     * @param Mailable|string $detail
     * @return bool
     */
    public static function send(string $value, VerificationTypeEnum $type, Mailable|string $detail): bool
    {
        $item = self::getToken($value, $type);

        if ($item->fresh) {
            /** @var TypeInterface $class */
            $class = new ($type->class())();

            $detail = $type->add($item->token, $detail);
            return $class->send($value, $detail);
        }

        return true;
    }
}
