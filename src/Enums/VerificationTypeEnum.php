<?php

namespace MahanShoghy\LaravelVerification\Enums;

use Illuminate\Mail\Mailable;
use MahanShoghy\LaravelVerification\Types\EmailType;
use MahanShoghy\LaravelVerification\Types\SmsType;
use MahanShoghy\PhpEnumHelper\EnumHelper;

enum VerificationTypeEnum: string
{
    use EnumHelper;

    case EMAIL = 'email';
    case SMS = 'sms';

    public function class(): string
    {
        return match ($this){
            self::EMAIL => EmailType::class,
            self::SMS => SmsType::class,
        };
    }

    public function add(string $token, Mailable|string $detail): Mailable|string
    {
        return match ($this){
            self::EMAIL => new $detail($token),

            self::SMS => str($detail)->replace(config('verification.sms_token_name'), $token),
        };
    }
}
