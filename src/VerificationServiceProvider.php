<?php

namespace MahanShoghy\LaravelVerification;

use Illuminate\Support\ServiceProvider;

class VerificationServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/Migrations');

        $this->mergeConfigFrom(__DIR__.'/Config/verification.php', 'verification');
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {

            $this->publishes([
                __DIR__.'/Config/verification.php' => config_path('verification.php')
            ], 'config');
        }
    }
}
