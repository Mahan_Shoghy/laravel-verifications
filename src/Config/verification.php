<?php

return [
    'token_length' => 6,

    'expiration_seconds' => 120,

    'max_attempt' => 3,

    'sms_token_name' => ':token',

    'table' => 'verification_tokens',
];
