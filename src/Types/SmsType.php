<?php

namespace MahanShoghy\LaravelVerification\Types;

use Illuminate\Mail\Mailable;
use Tzsk\Sms\Facades\Sms;

class SmsType implements TypeInterface
{
    public function send(string $value, Mailable|string $detail): bool
    {
        Sms::send($value)->to($detail)->dispatch();

        return true;
    }
}
