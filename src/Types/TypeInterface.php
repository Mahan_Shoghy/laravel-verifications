<?php

namespace MahanShoghy\LaravelVerification\Types;


use Illuminate\Mail\Mailable;

interface TypeInterface
{
    public function send(string $value, Mailable|string $detail): bool;
}
