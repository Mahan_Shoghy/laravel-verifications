<?php

namespace MahanShoghy\LaravelVerification\Types;

use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;

class EmailType implements TypeInterface
{
    public function send(string $value, Mailable|string $detail): bool
    {
        Mail::to($value)->queue($detail);

        return true;
    }
}
